# Proyek Akhir Semester E07

**Kelompok E07**
Anggota:

- Evan Christopher (2006597065)
- Fatima Azzahra Triputra (2006597544)
- Lucius Rianto (2006596730)
- Kelvin Erlangga (2006596964)
- Kevin Xavier Emil Aryade Kalsim (2006596756)
- Muhammad Adrian Wirakusumah (2006487351)
- Shabrina Nurmalitasari (2006596705)


**Link menuju APK (Sementara)** : https://drive.google.com/file/d/13IzACCKXF4sVxqztukDDMRNTp5fu7jPm/view?usp=sharing

**Daftar modul yang akan diimplementasikan dan integrasinya dengan webservice:**

- Registration page : **POST** (Adrian)
1. Menerapkan Form untuk mengambil hasil input user (Django)
2. Redirect ke login page setelah berhasil registrasi
- Login page : **POST** (Lucius)
1. Menerapkan Form untuk mengambil hasil input user (Django)
2. Akan melakukan redirect ke Home Page jika Login berhasil
- Home page + Headline news seputar COVID-19 : **GET**
1. Implementasi widget yang berjalan pada waktu realtime untuk memberikan penjelasan singkat website
2. Implementasi widget yang berjalan pada waktu realtime untuk memberikan penjelasan singkat serta akses ke modul website  
lainnya, yaitu:
- modul info COVID-19
- modul info vaksin
- modul info apotik
- forum diskusi
3. Implementasi widget form untuk menepatkan form feedback serta memaparkan feed - feedback yang telah disampaikan
- Forum diskusi (**Form** and **Page View**) : **POST** (Fatima)

**Form**
1. Menerapkan form untuk menambahkan diskusi (Django)
2. Menerapkan form untuk memberi komentar (Django)
3. Redirect ke halaman forum diskusi setelah submit diskusi/komentar

**Page View**

1. Implementasi button yang akan redirect ke form untuk menambah diskusi
2. Menampilkan daftar diskusi yang sudah dikirim oleh user (jika ada)
3. Implementasi button untuk reply, edit dan delete pada diskusi
4. Implementasi button delete pada komentar
- COVID-19 Information page : **GET** (Kelvin)

**GET**
1. Menampilkan data kasus per hari COVID 19 dari API webserver django dalam bentuk diagram garis dengan widget
2. Menerapkan Form untuk mengambil nama provinsi hasil input user (Django)
3. Akan menampilkan data kasus kumulatif COVID-19 di provinsi tersebut
syncfusion_flutter_charts
- Apotik Information Page : **GET** (Evan)
1. Searchable dengan beberapa nama kota (Searchable - Ajax)
2. Mengeluarkan link yang menuju kepada website nomor telefon Apotik tersebut
3. Implementasi untuk modul ini akan dilakukan pemanggilan _Asynchronus_ ke Web Service Django berdasarkan kota mana yang
dicari, dan memanggil data dari database seperti informasi apotik tersebut.
- Vaccine Information Page : **GET** (Shabrina)
1. Searchable dengan nama vaksin (Ajax)
2. Mengeluarkan informasi mengenai vaksin; memanggil dari database untuk informasi vaksin tersebut.
