class CovidData {
  String date;
  int kasusPositif;
  int kasusMeninggal;
  int kasusSembuh;

  CovidData(
      {required this.date,
      required this.kasusPositif,
      required this.kasusMeninggal,
      required this.kasusSembuh});
}

class CovidProvinsi {
  String provinsi;
  int kumulatifPositif;
  int kumulatifMeninggal;
  int kumulatifSembuh;

  CovidProvinsi(
      {required this.provinsi,
      required this.kumulatifPositif,
      required this.kumulatifMeninggal,
      required this.kumulatifSembuh});
}
