class Vaccine {
  final String name;
  final String ingredient;
  final String minimumAge;
  final String dosesGap;

  Vaccine(this.name, this.ingredient, this.minimumAge, this.dosesGap);

  Vaccine.fromJson(Map<String, dynamic> json)
      : name = json['vaccine_name'],
        ingredient = json['ingredient'],
        minimumAge = json['minimum_age'],
        dosesGap = json['doses_gap'];

  Map<String, dynamic> toJson() => {
        'name': name,
        'ingredient': ingredient,
        'minimum_age': minimumAge,
        'doses_gap': dosesGap,
      };
}
