class Forum {
  String title;
  String author;
  String message;
  String date;

  Forum(
      {required this.title,
      required this.author,
      required this.message,
      required this.date});
}

class Comment {
  String post;
  String author;
  String reply;
  String repDate;

  Comment(
      {required this.post,
      required this.author,
      required this.reply,
      required this.repDate});
}
