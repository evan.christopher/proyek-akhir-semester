import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:covid_cares/models/disc_models.dart';
import 'package:covid_cares/widget/navigation_drawer_widget.dart';
import './disc_page.dart';
import 'package:http/http.dart' as http;

class EditPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => DiscPage()));
            },
          ),
        ),
        drawer: NavigationDrawerWidget(),
        backgroundColor: const Color(0xFF7ABECB),
        body: Center(child: EditForm()),
      ),
    );
  }
}

class EditForm extends StatefulWidget {
  const EditForm({Key? key}) : super(key: key);

  @override
  EditFormState createState() {
    return EditFormState();
  }
}

class EditFormState extends State<EditForm> {
  final _formKey = GlobalKey<FormState>();

  String title = "";
  String author = "";
  String message = "";
  String date = "";
  DateTime now = new DateTime.now();
  @override
  Widget build(BuildContext context) {
    date = '${now.year}-${now.month}-${now.day}, ${now.hour}:${now.minute}';
    return Center(
      child: Form(
          key: _formKey,
          child: Container(
            color: const Color(0xFF74cfbf),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              Padding(
                padding:
                    EdgeInsets.only(top: 22, bottom: 5, left: 30, right: 30),
                child: Text(
                  "Editing Post",
                  style: TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text("Title"),
              ),
              Padding(
                  padding: const EdgeInsets.only(
                      top: 5, bottom: 5, left: 30, right: 30),
                  child: Container(
                    color: Colors.white,
                    width: 250,
                    height: 40,
                    child: TextFormField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return null;
                          }
                          title = value;
                          return null;
                        }),
                  )),
              const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text("Message"),
              ),
              Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                    color: Colors.white,
                    width: 250,
                    height: 40,
                    child: TextFormField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return null;
                          }
                          message = value;
                          return null;
                        }),
                  )),
              Padding(
                padding: const EdgeInsets.only(top: 12.0, bottom: 22.0),
                child: TextButton(
                  child: const Text('Update Post'),
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      final response = await http.post(
                          Uri.parse(
                              "https://covid-cares.herokuapp.com/forum/get-forum"),
                          headers: <String, String>{
                            'Content-Type': 'application/json;charset=UTF-8',
                          },
                          body: jsonEncode(<String, String>{
                            'title': title,
                            'author': author,
                            'message': message,
                            'date': date,
                          }));
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Edited')),
                      );
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => DiscPage()),
                      );
                    }
                  },
                  style: TextButton.styleFrom(
                      backgroundColor: const Color(0xFFF7BA5B),
                      primary: Colors.white),
                ),
              )
            ]),
          )),
    );
  }
}
