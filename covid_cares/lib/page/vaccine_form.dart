import 'package:covid_cares/widget/navigation_drawer_widget.dart';
import 'package:flutter/material.dart';
import 'package:covid_cares/page/vaccine_detail.dart';

class FormPage extends StatefulWidget {
  const FormPage({Key? key}) : super(key: key);

  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  late String _vaccineName;

  final _formKey = GlobalKey<FormState>();

  Widget _buildVaccineName() {
    return TextFormField(
      decoration: const InputDecoration(labelText: 'Insert vaccine name'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please insert vaccine name';
        } else if (value.length < 5) {
          return 'Minimal input is 5 characters';
        }
      },
      onSaved: (value) {
        _vaccineName = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF7ABECC),
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "Vaccine Information Page",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: Colors.white,
        foregroundColor: Colors.grey[600],
      ),
      body: Center(
        child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
              constraints: const BoxConstraints(maxHeight: 200, maxWidth: 500),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _buildVaccineName(),
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ElevatedButton(
                          child: const Text('Search'),
                          onPressed: () {
                            if (!(_formKey.currentState!.validate())) {
                              return;
                            }
                            _formKey.currentState!.save();
                            // Send to API or next page?
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    DetailVaccinePage(query: _vaccineName),
                              ),
                            );
                          },
                        )),
                  ],
                ),
              ),
              color: Colors.white,
              padding: const EdgeInsets.all(20.0),
            )),
      ),
      drawer: NavigationDrawerWidget(),
    );
  }
}
