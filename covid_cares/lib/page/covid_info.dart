import 'package:flutter/material.dart';
import 'package:covid_cares/models/covid_models.dart';
import 'package:covid_cares/data/covid_data.dart';
import 'package:covid_cares/widget/navigation_drawer_widget.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class CovidInfoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ViewPageTab(),
    );
  }
}

class ViewPageTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          drawer: NavigationDrawerWidget(),
          appBar: AppBar(
            bottom: const TabBar(
              tabs: [
                Tab(
                  icon: Icon(Icons.stacked_line_chart_rounded),
                  text: "Line Chart",
                ),
                Tab(
                  icon: Icon(Icons.table_chart_rounded),
                  text: "Table View",
                ),
              ],
            ),
            backgroundColor: Colors.blue,
            centerTitle: true,
            title: const Text(
              'Covid Information Page',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          body: TabBarView(
            children: [
              GraphWidget(),
              FormKota(),
            ],
          ),
        ),
      ),
    );
  }
}

class GraphWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchData(),
        builder:
            (BuildContext context, AsyncSnapshot<List<CovidData>> dataList) {
          if (dataList.hasData) {
            return Container(
              padding: const EdgeInsets.all(10),
              child: SfCartesianChart(
                primaryXAxis: CategoryAxis(),
                title: ChartTitle(
                    text: "Statistik Kasus Harian Per Minggu COVID-19"),
                legend:
                    Legend(isVisible: true, position: LegendPosition.bottom),
                tooltipBehavior: TooltipBehavior(enable: true),
                series: <ChartSeries<CovidData, String>>[
                  LineSeries<CovidData, String>(
                    name: 'Kasus Positif',
                    dataSource: dataList.data!,
                    xValueMapper: (CovidData data, _) => data.date,
                    yValueMapper: (CovidData data, _) => data.kasusPositif,
                    markerSettings: const MarkerSettings(isVisible: true),
                  ),
                  LineSeries<CovidData, String>(
                    name: 'Kasus Meninggal',
                    dataSource: dataList.data!,
                    xValueMapper: (CovidData data, _) => data.date,
                    yValueMapper: (CovidData data, _) => data.kasusMeninggal,
                    markerSettings: const MarkerSettings(isVisible: true),
                  ),
                  LineSeries<CovidData, String>(
                    name: 'Kasus Sembuh',
                    dataSource: dataList.data!,
                    xValueMapper: (CovidData data, _) => data.date,
                    yValueMapper: (CovidData data, _) => data.kasusSembuh,
                    markerSettings: const MarkerSettings(isVisible: true),
                  )
                ],
              ),
            );
          } else if (dataList.hasError) {
            return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.error_outline,
                    color: Colors.red,
                    size: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Text('Error: ${dataList.error}'),
                  )
                ]);
          } else {
            return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    width: 60,
                    height: 60,
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Text('Awaiting result...'),
                  )
                ]);
          }
        });
  }
}

class FormKota extends StatefulWidget {
  @override
  _FormKotaState createState() => _FormKotaState();
}

class _FormKotaState extends State<FormKota> {
  final _formKey = GlobalKey<FormState>();
  final inputController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    inputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: inputController,
                    decoration: InputDecoration(
                      hintText: "Masukkan Nama Provinsi",
                      icon: const Icon(Icons.location_city),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5)),
                    ),
                    validator: (value) {
                      if (value == null) {
                        return 'Nama provinsi tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                ElevatedButton(
                  child: const Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blue),
                  ),
                  onPressed: () {
                    var pilihan = inputController.text.toString();
                    if (_formKey.currentState!.validate()) {
                      showDialog(
                          context: context,
                          barrierDismissible: false, // user must tap button!
                          builder: (BuildContext context) {
                            try {
                              return AlertDialog(
                                  actions: [
                                    new ElevatedButton(
                                      child: const Text("Ok"),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                  ],
                                  title: Container(
                                    child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: FutureBuilder(
                                            future: fetchProvinsi(pilihan),
                                            builder: (BuildContext context,
                                                AsyncSnapshot<CovidProvinsi?>
                                                    provdata) {
                                              if (provdata.connectionState ==
                                                  ConnectionState.done) {
                                                if (provdata.data == null) {
                                                  return Text(
                                                    'Provinsi Tidak Ditemukan',
                                                    style: const TextStyle(
                                                        color: Colors.red,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                    textAlign: TextAlign.center,
                                                  );
                                                } else {
                                                  return Text(
                                                    'Kasus Kumulatif Untuk Provinsi ${provdata.data!.provinsi}',
                                                    style: const TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                    textAlign: TextAlign.center,
                                                  );
                                                }
                                              } else {
                                                return const CircularProgressIndicator();
                                              }
                                            })),
                                  ),
                                  content: Container(
                                      height: 200,
                                      width: 300,
                                      child: Column(
                                        children: [
                                          FutureBuilder(
                                              future: fetchProvinsi(pilihan),
                                              builder: (BuildContext context,
                                                  AsyncSnapshot<CovidProvinsi?>
                                                      provdata) {
                                                if (provdata.connectionState ==
                                                    ConnectionState.done) {
                                                  if (provdata.data == null) {
                                                    return Text(
                                                      'Data Tidak Ditemukan Pada Database',
                                                      style: const TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                      textAlign:
                                                          TextAlign.center,
                                                    );
                                                  } else {
                                                    return Container(
                                                        height: 150,
                                                        child:
                                                            SingleChildScrollView(
                                                          scrollDirection:
                                                              Axis.vertical,
                                                          child: Column(
                                                              children: [
                                                                Container(
                                                                  height: 50.0,
                                                                  color: Colors
                                                                          .blue[
                                                                      600],
                                                                  margin:
                                                                      EdgeInsets
                                                                          .all(
                                                                              6.0),
                                                                  child: Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Row(
                                                                          mainAxisAlignment: MainAxisAlignment
                                                                              .spaceEvenly,
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.center,
                                                                          children: [
                                                                            Icon(Icons.warning,
                                                                                color: Colors.white),
                                                                            Center(
                                                                              child: Text(
                                                                                'Kasus Positif',
                                                                                style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                            ),
                                                                            Icon(Icons.warning,
                                                                                color: Colors.white),
                                                                          ]),
                                                                      Row(
                                                                          mainAxisAlignment: MainAxisAlignment
                                                                              .center,
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.center,
                                                                          children: [
                                                                            Text(
                                                                              provdata.data!.kumulatifPositif.toString(),
                                                                              style: TextStyle(color: Colors.white),
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ]),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  height: 50.0,
                                                                  color: Colors
                                                                      .red,
                                                                  margin:
                                                                      EdgeInsets
                                                                          .all(
                                                                              6.0),
                                                                  child: Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Row(
                                                                          mainAxisAlignment: MainAxisAlignment
                                                                              .spaceEvenly,
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.center,
                                                                          children: [
                                                                            Icon(Icons.dangerous,
                                                                                color: Colors.white),
                                                                            Center(
                                                                              child: Text(
                                                                                'Kasus Meninggal',
                                                                                style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                            ),
                                                                            Icon(Icons.dangerous,
                                                                                color: Colors.white),
                                                                          ]),
                                                                      Row(
                                                                          mainAxisAlignment: MainAxisAlignment
                                                                              .center,
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.center,
                                                                          children: [
                                                                            Text(
                                                                              provdata.data!.kumulatifMeninggal.toString(),
                                                                              style: TextStyle(color: Colors.white),
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ]),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                  height: 50.0,
                                                                  color: Colors
                                                                      .green,
                                                                  margin:
                                                                      EdgeInsets
                                                                          .all(
                                                                              6.0),
                                                                  child: Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Row(
                                                                          mainAxisAlignment: MainAxisAlignment
                                                                              .spaceEvenly,
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.center,
                                                                          children: [
                                                                            Icon(Icons.health_and_safety,
                                                                                color: Colors.white),
                                                                            Center(
                                                                              child: Text(
                                                                                'Kasus Sembuh',
                                                                                style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                            ),
                                                                            Icon(Icons.health_and_safety,
                                                                                color: Colors.white),
                                                                          ]),
                                                                      Row(
                                                                          mainAxisAlignment: MainAxisAlignment
                                                                              .center,
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.center,
                                                                          children: [
                                                                            Text(
                                                                              provdata.data!.kumulatifSembuh.toString(),
                                                                              style: TextStyle(color: Colors.white),
                                                                              textAlign: TextAlign.center,
                                                                            ),
                                                                          ]),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ]),
                                                        ));
                                                  }
                                                } else {
                                                  return const CircularProgressIndicator();
                                                }
                                              }),
                                          const Padding(
                                            padding: EdgeInsets.only(top: 30),
                                            child: Text(
                                              '* Geser ke atas dan ke bawah untuk melihat dengan detail',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 10,
                                                  fontStyle: FontStyle.italic),
                                            ),
                                          )
                                        ],
                                      )));
                            } catch (e) {
                              return AlertDialog(
                                title: Container(
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      'Error Fetching Data',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  color: Colors.red,
                                ),
                              );
                            }
                          });
                    } else {
                      return null;
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
