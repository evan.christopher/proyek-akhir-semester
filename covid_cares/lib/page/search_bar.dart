import 'package:flutter/material.dart';
import 'package:covid_cares/calling_data.dart';
import 'package:covid_cares/main.dart';
import 'package:covid_cares/models.dart';
import 'package:covid_cares/widget/navigation_drawer_widget.dart';
import 'package:covid_cares/page/user_page.dart';
import 'package:url_launcher/url_launcher.dart';

class SearchBar extends StatefulWidget {
  const SearchBar({Key? key}) : super(key: key);

  @override
  State<SearchBar> createState() => _SearchBar();
}

class _SearchBar extends State<SearchBar> {
  final name = 'Test';
  final email = '123@test.com';
  final urlImage =
      'https://toppng.com/free-image/cat-cat-cool-cat-cute-cat-face-wallpaper-PNG-free-PNG-Images_150032';
  String tampung = "";
  bool boolean = false;
  final _formKey = GlobalKey<FormState>();
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(0, 206, 209, 1),
          title: Text('Apotek'),
        ),
        backgroundColor: Colors.greenAccent,
        body: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 70.0),
                child: Icon(Icons.house_siding_outlined, size: 150, color: Colors.black45),
                
              ),
              Text("Yuk Cek Apotik di Kotamu! (ง •̀_•́)ง",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      height: 1,
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                      color: Color.fromARGB(120, 60, 50, 113))),
              
              TextFormField(
                  onChanged: (value) => {
                        if (value != '')
                          {
                            setState(
                              () {
                                tampung = value;
                              },
                            )
                          }
                      },
                  decoration: new InputDecoration(
                    hintText: 'Masukkan nama kotamu disini...',
                    labelText: 'Nama kota',
                    icon: Icon(Icons.assignment),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  validator: (context) {
                    if (context!.isEmpty) {
                      return 'Alamat tidak boleh kosong';
                    }
                    return null;
                  }
                  // validator: (value) {
                  //   if (value == null || value.isEmpty) {
                  //     return 'Please enter some text';
                  //   }
                  //   tampung = value;
                  //   return null;
                  // },
                  ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton.icon(
                  icon: Icon(Icons.search, size: 36, color: Colors.black45),
                  label: Text("Carikan dong!", style: TextStyle(color: Colors.black45, fontSize: 15, fontWeight: FontWeight.bold)),
                  onPressed: () {
                    // if (_formKey.currentState?.validate() ?? false) {
                    setState(() {
                      boolean = true;
                    });
                    // Respond to button press
                    // };
                  }),
              boolean
                  ? Expanded(
                      child: FutureBuilder<List<Apotik>>(
                        future: callingData(
                            tampung), // a previously-obtained Future<String> or null
                        builder: (context, snapshot) {
                          List<Widget> children;
                          if (snapshot.hasData) {
                            final List<Apotik>? apotik = snapshot.data;
                            if (apotik!.length > 0) {
                              return ListView.builder(
                                itemCount: apotik.length,
                                itemBuilder: (context, index) {
                                  return Column(children: [
                                    Text(apotik[index].daerah + ": ", style: TextStyle(height: 2)),
                                    Center(
                                        child: new InkWell(
                                            child: new Text(
                                                "Klik disini untuk cek Apotik di daerah " +
                                                    apotik[index].daerah +
                                                    ".", style: TextStyle(height: 2, fontWeight: FontWeight.bold, color: Colors.redAccent, decoration: TextDecoration.underline)),
                                            onTap: () =>
                                                launch(apotik[index].link)))
                                  ]);
                                },
                              );
                            } else {
                              return Text("Maaf kota yang Anda tuju tidak ada dalam daftar kami",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      height: 2,
                                      fontSize: 19,
                                      fontWeight: FontWeight.bold,
                                      color:
                                          Colors.red));
                            }
                          } else if (snapshot.hasError) {
                            children = <Widget>[
                              const Icon(
                                Icons.error_outline,
                                color: Colors.red,
                                size: 60,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 16),
                                child: Text('Error: ${snapshot.error}'),
                              )
                            ];
                          } else {
                            children = const <Widget>[
                              SizedBox(
                                width: 60,
                                height: 60,
                                child: CircularProgressIndicator(),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 16),
                                child: Text('Awaiting result...'),
                              )
                            ];
                          }
                          return Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: children,
                            ),
                          );
                        },
                      ),
                    )
                  : Text(''),
            ],
          ),
        ),
        drawer: NavigationDrawerWidget());
  }
}
