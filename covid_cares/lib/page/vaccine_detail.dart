import 'package:flutter/material.dart';
import 'package:covid_cares/models/vaccine_models.dart';
import 'package:covid_cares/fetch_vaccine.dart';

class DetailVaccinePage extends StatelessWidget {
  const DetailVaccinePage({Key? key, required this.query}) : super(key: key);

  final String query;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF7ABECC),
      appBar: AppBar(
        title: Text("Result for $query"),
        backgroundColor: Colors.white,
        foregroundColor: Colors.grey[850],
      ),
      body: FutureBuilder(
        future: fetchVaccineData(query),
        builder: (context, snapshot) {
          print(snapshot.data);
          if (snapshot.hasData &&
              (snapshot.data as Map<String, dynamic>).isNotEmpty) {
            print("ada");
            var vaccine =
                Vaccine.fromJson(snapshot.data as Map<String, dynamic>);
            return Center(
              child: Padding(
                padding: EdgeInsets.all(20.0),
                child: Container(
                  constraints: BoxConstraints(minHeight: 500, minWidth: 1000),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          vaccine.name,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          'Ingredient: \n' + vaccine.ingredient,
                          style: TextStyle(fontStyle: FontStyle.italic),
                          textAlign: TextAlign.center,
                        ),
                        Text('Dose Gap: ' + vaccine.dosesGap),
                        Text('Minimum Year: ' + vaccine.minimumAge),
                      ],
                    ),
                  ),
                  color: Colors.white,
                  padding: const EdgeInsets.all(50.0),
                ),
              ),
            );
          } else {
            return Center(
              child: Padding(
                padding: EdgeInsets.all(20.0),
                child: Container(
                  constraints: BoxConstraints(minHeight: 500, minWidth: 1000),
                  child: Center(
                    child: Text(
                      "Information not found",
                      style:
                          TextStyle(fontStyle: FontStyle.italic, fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  color: Colors.white,
                  padding: const EdgeInsets.all(50.0),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
