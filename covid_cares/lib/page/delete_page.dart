import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:covid_cares/models/disc_models.dart';
import 'package:covid_cares/widget/navigation_drawer_widget.dart';
import './disc_page.dart';
import 'package:http/http.dart' as http;

class DeletePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        drawer: NavigationDrawerWidget(),
        backgroundColor: const Color(0xFF7ABECB),
        body: Center(child: MyDeletePage()),
      ),
    );
  }
}

class MyDeletePage extends StatelessWidget {
  const MyDeletePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      width: 300,
      color: const Color(0xFF74cfbf),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        Padding(
          padding: EdgeInsets.only(top: 22, bottom: 10, left: 30, right: 30),
          child: Text(
            "Are You Sure?",
            style: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 1),
                fontWeight: FontWeight.bold,
                fontSize: 25),
          ),
        ),
        Padding(
            padding: EdgeInsets.only(top: 5, bottom: 5, left: 30, right: 30),
            child: Text(
              "Item will be permanently deleted.",
              style: TextStyle(fontSize: 15),
            )),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(top: 10.0, bottom: 22.0, right: 30.0),
              child: TextButton(
                child: const Text('Delete'),
                onPressed: () {/* ... */},
                style: TextButton.styleFrom(
                    backgroundColor: const Color(0xFFF44336),
                    primary: Colors.white),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 10.0, bottom: 22.0, left: 30.0),
              child: TextButton(
                child: const Text('Cancel'),
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => DiscPage()));
                },
                style: TextButton.styleFrom(
                    backgroundColor: const Color(0xFF9E9E9E),
                    primary: Colors.white),
              ),
            )
          ],
        ),
      ]),
    ));
  }
}
