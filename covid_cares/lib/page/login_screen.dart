import 'package:covid_cares/page/signup_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:covid_cares/main.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:convert';
import 'package:covid_cares/models/user_models.dart';
import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isRememberMe = false;
  final fetchUsername = TextEditingController();
  final fetchPassword = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    fetchUsername.dispose();
    fetchPassword.dispose();
    super.dispose();
  }

  Widget emailBox() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Covid Cares ID',
          style: TextStyle(
            color: Colors.black54,
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 6,
                  offset: Offset(0, 2),
                )
              ]),
          height: 60,
          child: TextField(
            controller: fetchUsername,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              color: Colors.black87,
            ),
            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14),
                prefixIcon: Icon(
                  Icons.account_box,
                  color: Colors.black,
                ),
                hintText: 'Your Covid Cares ID',
                hintStyle: TextStyle(
                  color: Colors.black38,
                )),
          ),
        )
      ],
    );
  }

  Widget passwordBox() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Password',
          style: TextStyle(
            color: Colors.black54,
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 6,
                  offset: Offset(0, 2),
                )
              ]),
          height: 60,
          child: TextField(
            controller: fetchPassword,
            obscureText: true,
            style: TextStyle(
              color: Colors.black87,
            ),
            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14),
                prefixIcon: Icon(
                  Icons.lock,
                  color: Colors.black,
                ),
                hintText: 'Never give your password to anyone',
                hintStyle: TextStyle(
                  color: Colors.black38,
                )),
          ),
        )
      ],
    );
  }

  Widget loginBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5,
        onPressed: () async {
          var username = fetchUsername.text;
          var password = fetchPassword.text;

          if (username.length != 0 && password.length != 0) {
            if (currentUser != null) {
              showDialog(
                  context: context,
                  barrierDismissible: false, // user must tap button!
                  builder: (BuildContext context) {
                    return AlertDialog(
                      actions: [
                        new ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.red),
                              padding:
                                  MaterialStateProperty.all(EdgeInsets.all(50)),
                              textStyle: MaterialStateProperty.all(
                                  TextStyle(fontSize: 30))),
                          child: const Text("Yes"),
                          onPressed: () async {
                            final logout = await http.get(
                              Uri.parse(
                                  "https://covid-cares.herokuapp.com/accounts/login/logout-flutter/"),
                              headers: <String, String>{
                                'Content-Type':
                                    'application/json;charset=UTF-8',
                              },
                            );
                            currentUser = null;
                            selectedItem(context, 0);
                          },
                        ),
                        new ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.grey),
                              padding:
                                  MaterialStateProperty.all(EdgeInsets.all(50)),
                              textStyle: MaterialStateProperty.all(
                                  TextStyle(fontSize: 30))),
                          child: const Text("No"),
                          onPressed: () => selectedItem(context, 0),
                        ),
                      ],
                      title: Text(
                        "You Are Still Logged In",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                      content: Text("Do you want to log out?"),
                    );
                  });
            } else {
              final response = await http.post(
                  Uri.parse(
                      "https://covid-cares.herokuapp.com/accounts/login/login-flutter/"),
                  headers: <String, String>{
                    'Content-Type': 'application/json;charset=UTF-8',
                  },
                  body: jsonEncode(<String, String>{
                    'username': username,
                    'password': password,
                  }));
              var result = jsonDecode(response.body);

              if (result['errors'] != null) {
                showDialog(
                    context: context,
                    barrierDismissible: false, // user must tap button!
                    builder: (BuildContext context) {
                      return AlertDialog(
                        actions: [
                          new ElevatedButton(
                            child: const Text("Ok"),
                            onPressed: () => Navigator.pop(context),
                          ),
                        ],
                        title: Text(
                          "Error",
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        content: Text("Wrong Username or Password"),
                      );
                    });
              } else {
                showDialog(
                    context: context,
                    barrierDismissible: false, // user must tap button!
                    builder: (BuildContext context) {
                      currentUser = new User(
                          username: result['username'], id: result['id']);
                      return AlertDialog(
                        actions: [
                          new ElevatedButton(
                            child: const Text("Go To Home Page"),
                            onPressed: () => selectedItem(context, 0),
                          ),
                        ],
                        title: Text(
                          "Status",
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        content: Text("You Are Succesfully Logged In"),
                      );
                    });
              }
            }
          } else {
            showDialog(
                context: context,
                barrierDismissible: false, // user must tap button!
                builder: (BuildContext context) {
                  return AlertDialog(
                    actions: [
                      new ElevatedButton(
                        child: const Text("Ok"),
                        onPressed: () => Navigator.pop(context),
                      ),
                    ],
                    title: Text(
                      "Error",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    content: Text("Username and password must not empty"),
                  );
                });
          }
        },
        padding: EdgeInsets.all(15),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        color: Colors.white,
        child: Text(
          'Login',
          style: TextStyle(
              color: Color(0xFF07E8FD),
              fontSize: 18,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget signupBtn() {
    return GestureDetector(
      onTap: () => selectedItem(context, 1),
      child: RichText(
        text: TextSpan(children: [
          TextSpan(
              text: 'Don\'t have a Covid Cares Account yet ?',
              style: TextStyle(
                color: Colors.black54,
                fontSize: 18,
                fontWeight: FontWeight.w500,
              )),
          TextSpan(
              text: ' Join now for free!',
              style: TextStyle(
                color: Colors.black54,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ))
        ]),
      ),
    );
  }

  void selectedItem(BuildContext context, int index) {
    Navigator.of(context).pop();

    switch (index) {
      case 0:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => MainPage(),
        ));
        break;
      case 1:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => SignUpScreen(),
        ));
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Stack(
          children: <Widget>[
            Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                    Color(0xFFBEF9FC),
                    Color(0xFF07E8FD),
                  ])),
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                padding: EdgeInsets.symmetric(horizontal: 25, vertical: 120),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Covid Cares',
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 40,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 25),
                    SvgPicture.asset('assets/icons/login.svg'),
                    SizedBox(height: 50),
                    emailBox(),
                    SizedBox(height: 25),
                    passwordBox(),
                    SizedBox(height: 10),
                    loginBtn(),
                    signupBtn(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
