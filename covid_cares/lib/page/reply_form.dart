import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:covid_cares/models/disc_models.dart';
import 'package:covid_cares/widget/navigation_drawer_widget.dart';
import './disc_page.dart';
import 'package:http/http.dart' as http;

class ReplyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => DiscPage()));
            },
          ),
        ),
        drawer: NavigationDrawerWidget(),
        backgroundColor: const Color(0xFF7ABECB),
        body: Center(child: ReplyForm()),
      ),
    );
  }
}

class ReplyForm extends StatefulWidget {
  const ReplyForm({Key? key}) : super(key: key);

  @override
  ReplyFormState createState() {
    return ReplyFormState();
  }
}

class ReplyFormState extends State<ReplyForm> {
  final _formKey = GlobalKey<FormState>();

  String post = "";
  String author = "";
  String reply = "";
  String repDate = "";
  DateTime now = new DateTime.now();
  @override
  Widget build(BuildContext context) {
    repDate = '${now.year}-${now.month}-${now.day}, ${now.hour}:${now.minute}';
    return Center(
      child: Form(
          key: _formKey,
          child: Container(
            color: const Color(0xFF74cfbf),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              Padding(
                padding:
                    EdgeInsets.only(top: 22, bottom: 5, left: 30, right: 30),
                child: Text(
                  "Add a Reply",
                  style: TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text("Message*"),
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 30, right: 30),
                  child: Container(
                    color: Colors.white,
                    width: 250,
                    height: 40,
                    child: TextFormField(
                        decoration: const InputDecoration(
                          hintMaxLines: 7,
                          border: OutlineInputBorder(),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Field cannot be empty";
                          }
                          reply = value;
                          return null;
                        }),
                  )),
              Padding(
                padding: const EdgeInsets.only(top: 12.0, bottom: 22.0),
                child: TextButton(
                  child: const Text('Reply'),
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      final response = await http.post(
                          Uri.parse(
                              "https://covid-cares.herokuapp.com/forum/get-reply"),
                          headers: <String, String>{
                            'Content-Type': 'application/json;charset=UTF-8',
                          },
                          body: jsonEncode(<String, String>{
                            'post': post,
                            'author': author,
                            'reply': reply,
                            'repDate': repDate,
                          }));
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Reply Added')),
                      );
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => DiscPage()),
                      );
                    }
                  },
                  style: TextButton.styleFrom(
                      backgroundColor: const Color(0xFFF7BA5B),
                      primary: Colors.white),
                ),
              )
            ]),
          )),
    );
  }
}
