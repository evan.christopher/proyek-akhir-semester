import 'package:flutter/material.dart';
import '../main.dart';
import '../models/disc_models.dart';
import '../widget/navigation_drawer_widget.dart';
import './disc_form.dart';
import './reply_form.dart';
import './delete_page.dart';
import './edit_form.dart';

class DiscPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => MainPage()));
          },
        ),
      ),
      drawer: NavigationDrawerWidget(),
      backgroundColor: const Color(0xFF7ABECB),
      body: Center(child: ForumPage()),
    ));
  }
}

class ForumPage extends StatelessWidget {
  const ForumPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          // FutureBuilder(
          //           future: fetchKegiatan(),
          //           builder: (context, snapshot) {
          //             if (snapshot.hasData) {
          //               List<Kegiatan>? kegiatan =
          //                   snapshot.data as List<Kegiatan>;
          //               return kegiatan.isEmpty
          //                   ? const Center(
          //                       child: Padding(
          //                         padding: EdgeInsets.only(top: 30.0),
          //                         child: Text(
          //                           'Belum ada kegiatan yang kamu tambahkan.',
          //                           style: TextStyle(
          //                               fontWeight: FontWeight.bold,
          //                               fontSize: 20),
          //                         ),
          //                       ),
          //                     )
          //                   : ListView.builder(
          //                       scrollDirection: Axis.vertical,
          //                       shrinkWrap: true,
          //                       itemCount: kegiatan.length,
          //                       itemBuilder: (context, index) {
          //                         return KegiatanCard(kegiatan[index]);
          //                       },
          //                     );
          //             } else {
          //               return const Center(child: CircularProgressIndicator());
          //             }
          //           }),
          //     ),
          Padding(
              padding: const EdgeInsets.only(top: 30.0, bottom: 20.0),
              child: SizedBox(
                width: 145,
                height: 35,
                child: ElevatedButton(
                  child: const Text("Add A Discussion"),
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => FormPage()));
                  },
                  style: TextButton.styleFrom(
                      backgroundColor: const Color(0xFFF7BA5B),
                      primary: Colors.white),
                ),
              )),
          Padding(
            padding: const EdgeInsets.only(right: 10.0, left: 10.0),
            child: SizedBox(
              width: 700,
              child: Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      visualDensity: VisualDensity(vertical: -4),
                      title: Text(
                        '1st discussion',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text('by fatima | Nov. 17, 2021, 03:47 p.m.'),
                    ),
                    const ListTile(
                      visualDensity: VisualDensity(vertical: -4),
                      title: Text(
                        'first forum text!!!',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        TextButton(
                          child: const Text('Reply'),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => ReplyPage()));
                          },
                          style: TextButton.styleFrom(
                              backgroundColor: const Color(0xFF74cfbf),
                              primary: Colors.white),
                        ),
                        const SizedBox(width: 3),
                        TextButton(
                          child: const Text('Edit'),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => EditPage()));
                          },
                          style: TextButton.styleFrom(
                              backgroundColor: Colors.grey[600],
                              primary: Colors.white),
                        ),
                        const SizedBox(width: 3),
                        TextButton(
                          child: const Text('Delete'),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => DeletePage()));
                          },
                          style: TextButton.styleFrom(
                              backgroundColor: Colors.red,
                              primary: Colors.white),
                        ),
                        const SizedBox(width: 10),
                      ],
                    ),
                    const Divider(
                      color: Colors.grey,
                      height: 20,
                      thickness: 0.5,
                      indent: 10,
                      endIndent: 10,
                    ),
                    Container(
                        color: Colors.grey[300],
                        width: 590,
                        margin: const EdgeInsets.only(bottom: 10),
                        child: Column(
                          children: [
                            const ListTile(
                              title: Text(
                                'fatima - Nov. 18, 2021, 06:24 p.m.',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 12),
                              ),
                              subtitle: Text('wow ada komen'),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Padding(
                                  padding:
                                      EdgeInsets.only(bottom: 10, right: 10),
                                  child: TextButton(
                                    child: const Text('Delete'),
                                    onPressed: () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DeletePage()));
                                    },
                                    style: TextButton.styleFrom(
                                      primary: Color.fromRGBO(0, 0, 255, 1),
                                    ),
                                  ),
                                  // Text(
                                  //   'Delete',
                                  //   style: TextStyle(
                                  //       color: Color.fromRGBO(0, 0, 255, 1),
                                  //       fontSize: 13),
                                  // ),
                                )
                              ],
                            )
                          ],
                        ))
                  ],
                ),
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(right: 10.0, left: 10.0),
            child: SizedBox(
              width: 700,
              child: Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      visualDensity: VisualDensity(vertical: -4),
                      title: Text(
                        '2nd post',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text('by fatima | Nov. 18, 2021, 04:01 p.m.'),
                    ),
                    const ListTile(
                      visualDensity: VisualDensity(vertical: -4),
                      title: Text(
                        'these are dummy datas',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        TextButton(
                          child: const Text('Reply'),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => ReplyPage()));
                          },
                          style: TextButton.styleFrom(
                              backgroundColor: const Color(0xFF74cfbf),
                              primary: Colors.white),
                        ),
                        const SizedBox(width: 3),
                        TextButton(
                          child: const Text('Edit'),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => EditPage()));
                          },
                          style: TextButton.styleFrom(
                              backgroundColor: Colors.grey[600],
                              primary: Colors.white),
                        ),
                        const SizedBox(width: 3),
                        TextButton(
                          child: const Text('Delete'),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => DeletePage()));
                          },
                          style: TextButton.styleFrom(
                              backgroundColor: Colors.red,
                              primary: Colors.white),
                        ),
                        const SizedBox(width: 10),
                      ],
                    ),
                    const Divider(
                      color: Colors.grey,
                      height: 20,
                      thickness: 0.5,
                      indent: 10,
                      endIndent: 10,
                    ),
                    Container(
                      color: Colors.grey[300],
                      width: 590,
                      margin: const EdgeInsets.only(bottom: 10),
                      child: const ListTile(
                        title: Text(
                          'Add a Reply',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 12),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// Future<List<Forum>> getForum() async {
//     final request = 
//     String url = 'https://covid-cares.herokuapp.com/forum/get-forum';

//     final response = await request.get(url);

//     List<Forum> result = [];
//     for (var d in response) {
//       if (d != null) {
//         result.add(Forum.);
//       }
//     }

//     return result;
//   }