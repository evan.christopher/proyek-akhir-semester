import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:covid_cares/models.dart';

Future<List<Apotik>> callingData(String query) async {
  var url = Uri.parse(
     'https://covid-cares.herokuapp.com/apotik/search_json?inputan=$query');
  print("owkoawk");
  print(query);
  var response = await http.get(
    url,
    headers: {
      // Required for cookies, authorization headers with HTTPS
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    },
  );
  print(response.body);
  var data = jsonDecode(utf8.decode(response.bodyBytes));
  List<Apotik> apotik = [];
  for (var d in data) {
    if (d != null) {
      apotik.add(Apotik.fromJson(d));
    }
  }
  return apotik;
}
