class Apotik {
  late String daerah;
  late String link;
  

  Apotik({
    required this.daerah,
    required this.link,
  });

  factory Apotik.fromJson(Map<String, dynamic> json) => Apotik(
    daerah: json["fields"]["daerah"],
    link: json["fields"]["link"],
  );

  Map<String, dynamic> toJson() => {
    "daerah": daerah,
    "link": link,
    
  };

}