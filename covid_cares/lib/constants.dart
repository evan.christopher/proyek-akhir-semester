import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF07E8FD);
const kPrimaryLightColor = Color(0xFFBEF9FC);
