import 'package:http/http.dart' as http;
import 'dart:convert';

Future<Map<String, dynamic>> fetchVaccineData(String query) async {
  // var url = 'http://127.0.0.1:8000/vaccine-info/search?vaccine_name=$query';
  var url =
      'https://covid-cares.herokuapp.com/vaccine-info/search?vaccine_name=$query';
  try {
    final response = await http.get(
      Uri.parse(url),
    );
    // print(response.body);
    Map<String, dynamic> extractedData = jsonDecode(response.body);
    if (extractedData.isNotEmpty) {
      return extractedData;
    }
  } catch (error) {
    print(error);
  }
  return {};
}
