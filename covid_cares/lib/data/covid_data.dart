import 'package:covid_cares/models/covid_models.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<CovidProvinsi?> fetchProvinsi(String query) async {
  CovidProvinsi? provinsi = null;
  var url =
      'https://covid-cares.herokuapp.com/covid-info/fetch-provinsi?query=$query';
  try {
    final response = await http.get(Uri.parse(url), headers: {
      // Required for cookies, authorization headers with HTTPS
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    });
    var extractedData = jsonDecode(response.body);
    if (extractedData['errors'] != null) {
      return null;
    }
    provinsi = CovidProvinsi(
        provinsi: extractedData['provinsi'],
        kumulatifPositif: extractedData['positif'],
        kumulatifMeninggal: extractedData['meninggal'],
        kumulatifSembuh: extractedData['sembuh']);
    return provinsi;
  } catch (error) {
    print(error);
    return null;
  }
}

Future<List<CovidData>> fetchData() async {
  List<CovidData> dataIndonesia = [];
  List<String> label;
  List<int> kasusPositif;
  List<int> kasusMeninggal;
  List<int> kasusSembuh;
  const url = 'https://covid-cares.herokuapp.com/covid-info/api-indonesia/';
  try {
    final response = await http.get(
      Uri.parse(url),
      headers: {
        // Required for cookies, authorization headers with HTTPS
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
      },
    );
    Map<String, dynamic> extractedData = jsonDecode(response.body);
    label = extractedData['labels'].cast<String>();
    kasusPositif = extractedData['dataPositif'].cast<int>();
    kasusMeninggal = extractedData['dataMeninggal'].cast<int>();
    kasusSembuh = extractedData['dataRecovery'].cast<int>();
    dataIndonesia = [
      CovidData(
          date: label[0],
          kasusPositif: kasusPositif[0],
          kasusMeninggal: kasusMeninggal[0],
          kasusSembuh: kasusSembuh[0]),
      CovidData(
          date: label[1],
          kasusPositif: kasusPositif[1],
          kasusMeninggal: kasusMeninggal[1],
          kasusSembuh: kasusSembuh[1]),
      CovidData(
          date: label[2],
          kasusPositif: kasusPositif[2],
          kasusMeninggal: kasusMeninggal[2],
          kasusSembuh: kasusSembuh[2]),
      CovidData(
          date: label[3],
          kasusPositif: kasusPositif[3],
          kasusMeninggal: kasusMeninggal[3],
          kasusSembuh: kasusSembuh[3]),
      CovidData(
          date: label[4],
          kasusPositif: kasusPositif[4],
          kasusMeninggal: kasusMeninggal[4],
          kasusSembuh: kasusSembuh[4]),
      CovidData(
          date: label[5],
          kasusPositif: kasusPositif[5],
          kasusMeninggal: kasusMeninggal[5],
          kasusSembuh: kasusSembuh[5]),
      CovidData(
          date: label[6],
          kasusPositif: kasusPositif[6],
          kasusMeninggal: kasusMeninggal[6],
          kasusSembuh: kasusSembuh[6]),
    ];
  } catch (error) {
    print(error);
  }
  return dataIndonesia;
}
